# JG Individual Project 2022

Repository for all code related to COMP 3931 Individual Project.

To run this program on an ubuntu terminal:

CD into the project directory, actiavte the virtual environment via the commmand 'source flask/bin/activate', you must also set the environment variables via
the terminal commands; 'FLASK_ENV=development', 'FLASK_APP=run.py'.

You may also have to pip install any missing libraries that the application depends on such as:

Package               Version
--------------------- -----------
bleach                5.0.0
certifi               2021.10.8
cffi                  1.15.0
charset-normalizer    2.0.12
click                 8.0.4
commonmark            0.9.1
cryptography          36.0.2
cycler                0.11.0
docutils              0.18.1
Flask                 2.0.3
Flask-WTF             1.0.0
fonttools             4.33.2
graphviz              0.20
idna                  3.3
importlib-metadata    4.11.3
itsdangerous          2.1.1
jeepney               0.8.0
Jinja2                3.0.3
keyring               23.5.0
kiwisolver            1.4.2
MarkupSafe            2.1.0
matplot               0.1.9
matplotlib            3.5.1
networkx              2.7.1
numpy                 1.22.3
packaging             21.3
Pillow                9.1.0
pip                   20.0.2
pkg-resources         0.0.0
pkginfo               1.8.2
pycparser             2.21
pydot                 1.4.2
Pygments              2.11.2
pyloco                0.0.139
pyparsing             3.0.8
python-dateutil       2.8.2
readme-renderer       35.0
requests              2.27.1
requests-toolbelt     0.9.1
rfc3986               2.0.0
rich                  12.2.0
SecretStorage         3.3.2
setuptools            44.0.0
SimpleWebSocketServer 0.1.1
six                   1.16.0
soupsieve             2.3.2.post1
twine                 4.0.0
typing                3.7.4.3
typing-extensions     4.2.0
urllib3               1.26.9
ushlex                0.99.1
webencodings          0.5.1
websocket-client      1.3.2
Werkzeug              2.0.3
wheel                 0.37.1
WTForms               3.0.1
zipp                  3.8.0

