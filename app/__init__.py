from flask import Flask
import os

path = os.getcwd()
path = path+"\\app\\temp\\webpages\\"
if not os.path.exists(path):
  os.mkdir(path)

app = Flask(__name__)
app.config.from_object('config')

from app import views