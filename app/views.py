from urllib.error import URLError
from flask import Flask, render_template, flash, redirect, session, request, make_response
from app import app
import pydot
from networkx.drawing.nx_pydot import *
import os
import urllib.request
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt


websiteGraph = nx.Graph() #This stores the graph of web pages and their links in the form of a list of lists, where ["www.bbc.com/home", "www.bbc.com/faq"] would indicate an edge going from the former to the latter
listOfExploredURLs = [] # Keeps track of which pages have already been explored to prevent infinite looping

@app.route('/')
def homepage():
  return render_template('home.html', title = 'Sitemap Generator')

@app.route('/SearchEngine')
def searchEngine():
  return render_template('searchEngine.html', title = 'Search Engine')

@app.route('/SubmitURLSearchEngine', methods=['POST', 'GET'])
def SearchFunction():
  listOfExploredURLs.clear()
  if request.method=='POST':
    if not request.form.get('inputURL'):
      response = 'Enter a URL'
    elif not request.form.get('inputTerms'):
      response = 'Enter Some Search Terms'
    elif not request.form.get('inputURL') and not request.form.get('inputTerms'):
      response = 'Enter a URL and Search Terms'
    else :
      try:
        
        generateSitemap()

        results = []
        pagesFound = 0
        termCount = 0
        termsFound = []
        rankedResults = []

        IT = request.form.get('inputTerms')
        searchTerms = IT.split(',')
        
        for url in listOfExploredURLs:
          try:
            urlResponse = urllib.request.urlopen(url) 
            pageContent = urlResponse.read().decode('UTF-8') #Obtains a text form of the webpage for exploration  

            for term in searchTerms:
              if term in pageContent:
                termCount = termCount + pageContent.count(term)
            if termCount > 0:
              results.append({"Index":pagesFound, "url":url, "termCount":termCount})
              pagesFound = pagesFound + 1
              termCount = 0
          except:
            continue

        if len(results) > 0:
          highestCount = 0
          rankedResults = []
          indexesAdded = []
          while len(rankedResults) < len(results) and len(rankedResults) < 5:
            for index in range(len(results)):
              if results[index]['termCount'] > highestCount:
                if results[index]['Index'] not in indexesAdded :
                  highestCount = results[index]['termCount']
                  temp = results[index]    
                  tempIndex=index
                  indexesAdded.append(results[index]['Index'])
            rankedResults.append(temp)
            highestCount = 0

          for index in range(len(rankedResults)):
            rankedResults[index]['Index'] = index
          
        print("Ranked Results ------------")
        print(rankedResults)
        response = "Success"
        return render_template('searchResults.html', title = 'Search Engine', results = rankedResults)      
        
      except Exception as e:
        response = "Please Enter a Valid URL, e.g. 'http://www.google.com/'. Search terms must be seperated by a comma"
        print(e)
        return render_template('searchEngine.html', title = 'Search Engine') 

@app.route('/displaySitemap')
def displayVisualSitemap():
  if len(listOfExploredURLs) == 0:
    response = "Error: Submit a URL First"
    return render_template('home.html', response=response)
  else:
    if os.path.exists("app/static/Sitemap.png"):
      os.remove("app/static/Sitemap.png")
    plt.clf()

    Sitemap = nx.bfs_tree(websiteGraph, source=listOfExploredURLs[0])
    plt.figure(5, figsize=(10,10))
    nx.draw(Sitemap, with_labels=True, node_size=50, font_size=7)
    plt.savefig('app/static/Sitemap.png')
    response = "Successfully Generated Sitemap"

    results = []
    count = 0
    for url in listOfExploredURLs:
      results.append({"Index":count, "Url": url})
      count = count + 1

    return render_template('Sitemap.html', response=response, results=results)

@app.route('/displayXMLSitemap', methods=['GET'])
def displayXMLSitemap():
  response = "Success"
  try:
    if len(listOfExploredURLs) == 0:
      response = "Error: No map data loaded"
    elif len(listOfExploredURLs) > 50000:
      response = "Error: URL limit exceeded for XML Sitemap Standard"
    else:
      return render_template('xmlSitemap.html', response=response, url=listOfExploredURLs)
  except Exception as e:
    response = "Error in Displaying XML Sitemap"
    print(e)
    return render_template('home.html', response=response)

@app.route('/clearSitemapData', methods=['GET', 'POST'])
def clearSiteMap():
  listOfExploredURLs.clear()
  websiteGraph.clear()
  if os.path.exists("app/static/UOGraph.png"):
    os.remove("app/static/UOGraph.png")
  if os.path.exists("app/static/Sitemap.png"):
    os.remove("app/static/Sitemap.png")
  response = "Sitemap Data Cleared"
  return render_template('home.html', response=response)

@app.route('/displayUOWebsiteGraph', methods=['GET'])
def displayUOWebsiteGraph():
  if len(listOfExploredURLs) == 0:
    response = "Error: Submit a URL First"
    return render_template('home.html', response=response)
  else:
    if os.path.exists("app/static/UOGraph.png"):
      os.remove("app/static/UOGraph.png")

    plt.clf()
    plt.figure(5, figsize=(10,10))
    nx.draw(websiteGraph, with_labels=True, node_size=50, font_size=7)


    plt.savefig("app/static/UOGraph.png", format="PNG")

    results = []
    count = 0
    for url in listOfExploredURLs:
      results.append({"Index":count, "Url": url})
      count = count + 1

    response = "Successfully Generated Website Graph"
    return render_template('UOWebsiteGraph.html', response=response, results = results)

@app.route('/SubmitURL', methods=['GET', 'POST'])
def generateSitemap():
  listOfExploredURLs.clear()
  websiteGraph.clear()

  if request.method=='POST':
    if not request.form.get('inputURL'):
      response = 'Enter a URL'
    else :
      try:

        
        count = 0
        url = '' # URL to be added to URLList
        listOfExploredURLs.append(request.form.get('inputURL')) 
        URLCount = 0 # Keeps index of which URL to Explore Next
        foundURL = 0 # Helps with skipping parts of searching whilst adding a url

        oldURLNum = 0 
        for selectedUrl in listOfExploredURLs:

          try:
            urlResponse = urllib.request.urlopen(selectedUrl) 
            pageContent = urlResponse.read().decode('UTF-8') #Obtains a text form of the webpage for exploration  
          except:
            continue

          currentURL = listOfExploredURLs[URLCount]
          i = 0

          while i < len(pageContent):
              

            if foundURL == 0:
              if pageContent[i] == 'a' and pageContent[i+1] == ' ' and pageContent[i+2]=='h' and pageContent[i+3]=='r' and pageContent[i+4]=='e' and pageContent[i+5]=='f' and pageContent[i+6]=='=':
                foundURL = 1
                i = i + 8    
                  

              
            if foundURL == 1:
              if pageContent[i] == '"' or pageContent[i] == "'":
                foundURL = 0

                if listOfExploredURLs[0] not in url and "http" in url: # THIS CHECKS IF URL IS OUTSIDE WEBSITE DOMAIN
                  url = ""
                  continue
                else:
                  if "http" not in url: #THIS CHECKS FOR RELATIVE URLS OR THOSE STARTING WITH '#'
                    if url[0] == '#' or (url[0] == '/' and url[1] == '#'): #IGNORE THESE FOR AUTOMATION EASE
                      url = ""
                      continue
                    else:
                      if url[0] == '/':
                        url = listOfExploredURLs[0] + url[1:] #TURN LOCAL FOLDER URL INTO ABSOLUTE URL
                      else:
                        url = listOfExploredURLs[0] + url
                        
                    

                  if listOfExploredURLs[0] not in url: # Eliminates dodgy URLS that fall through the checker
                    url = ""
                    continue

                  if listOfExploredURLs.count(url) == 0: # If testing and you see more than on copy of a URL, it may be that the page has 2 links with the same URL
                    listOfExploredURLs.append(url)
                    websiteGraph.add_node(url)
                    #websiteGraph.append([currentURL, url])
                    websiteGraph.add_edge(currentURL, url)
                      
                    url = ""
                  else:
                    #websiteGraph.append([currentURL, url]) # THIS MAY STILL END UP APPENDING THE SAME LINKS  ----- need TO PUT ALL THIS IN LOOP WHICH LOOKS FOR NEXT URLS AND ASKS FOR REPSONSE
                    websiteGraph.add_edge(currentURL, url)
                    url = ""  
              else:
                url = url + pageContent[i]
            
            i = i + 1

            if i == len(pageContent):
              URLCount = URLCount + 1
              foundURL = 0

          #if URLCount == 1: # WEBSITE MAPS TOO BIG TO DISPLAY WHEN TESTING REMOVE THIS IN FINAL VERSION 
          #  break
              
        response = "Success"
      except Exception as e:
        response = "Please Enter a Valid URL, e.g. 'http://www.google.com/'"
        print(e)

      


  return render_template('home.html', response=response)
